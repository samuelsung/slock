/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nogroup";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#2e3440",   /* after initialization */ // nord0
	[INPUT] =  "#5e81ac",   /* during input */ // nord10
	[FAILED] = "#bf616a",   /* wrong password */ // nord11
	[CAPS] =   "#d08770",   /* CapsLock on */ // nord12
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
