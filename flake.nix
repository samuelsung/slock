{
  description = "samuelsung's build of slock";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    (flake-utils.lib.eachSystem
      [
        "aarch64-linux"
        "i686-linux"
        "x86_64-linux"
      ]
      (system:
        let
          name = "slock";
          pkgs = nixpkgs.legacyPackages.${system};
          package = pkgs.slock.overrideAttrs (_: {
            version = "master";
            src = ./.;
            patches = [ ];
          });
        in
        rec {
          apps = { ${name} = { type = "app"; program = "${defaultPackage}/bin/${name}"; }; };
          defaultPackage = package;
          packages.${name} = defaultPackage;
          defaultApp = apps.${name};
        }
      )) // {
      overlay = final: prev: {
        slock = self.packages.${prev.system}.slock;
      };
    };
}
